
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

-- disable all mapgens
minetest.register_on_mapgen_init(function(parms)
	minetest.set_mapgen_params({mgname = "singlenode"})
end)

minetest.register_on_generated(function(minp, maxp)
    if minp.x <= 0 and minp.y <= -2 and minp.z <= 0 and maxp.x >= 0 and maxp.y >= -2 and maxp.z >= 0 then
      minetest.set_node({x = 0, y = -2, z = 0}, {name = "nodes:stone"})
    end
end)

-- squelch mapgen warnings
minetest.register_alias("mapgen_stone", "nodes:stone")
minetest.register_alias("mapgen_water_source", "nodes:water_source")
minetest.register_alias("mapgen_river_water_source", "nodes:river_water_source")
