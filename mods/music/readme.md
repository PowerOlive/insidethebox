##music mod for itb

This mod contains no music files. You should use the `localmusic`
worldmod to install music files on your server instead. See the
subfolder for that mod to see what needs to go in there and how.
